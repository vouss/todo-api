-- phpMyAdmin SQL Dump
-- version 4.2.5
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Czas generowania: 30 Maj 2015, 17:20
-- Wersja serwera: 5.5.38
-- Wersja PHP: 5.5.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Baza danych: `todo`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `descriptions`
--

CREATE TABLE `descriptions` (
`desc_id` int(10) unsigned NOT NULL,
  `task_id` int(10) unsigned NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=27 ;

--
-- Zrzut danych tabeli `descriptions`
--

INSERT INTO `descriptions` (`desc_id`, `task_id`, `text`) VALUES
(0, 0, 'Przykładowy opis zadania1'),
(1, 1, 'Przykładowy opis zadania2'),
(2, 2, 'Przykładowy opis zadania3'),
(3, 3, 'Przykładowy opis zadania4'),
(4, 4, 'Przykładowy opis zadania5'),
(6, 10, 'opis taska...'),
(7, 11, 'opis taska...'),
(8, 12, 'trollololo'),
(9, 13, 'trollololo'),
(10, 14, 'trollololo'),
(11, 15, 'trollololo'),
(12, 16, 'trollololo'),
(13, 17, 'trollololo'),
(14, 18, 'trollololo'),
(15, 19, 'trollololo'),
(16, 20, 'trollololo'),
(17, 21, 'trollololo'),
(18, 22, 'trollololo'),
(19, 23, 'opis taska...'),
(20, 24, 'trollololo'),
(21, 25, 'trollololo'),
(22, 26, 'trollololo'),
(23, 27, 'trollololo'),
(24, 28, 'trollololo'),
(25, 29, 'trollololo'),
(26, 30, 'trollololo');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_05_23_103539_status_table', 1),
('2015_05_23_103633_priorities_table', 1),
('2015_05_23_103706_task_table', 1),
('2015_05_23_103731_descriptions_table', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `priorities`
--

CREATE TABLE `priorities` (
  `text` text COLLATE utf8_unicode_ci NOT NULL,
`power` int(10) unsigned NOT NULL,
  `color` varchar(40) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Zrzut danych tabeli `priorities`
--

INSERT INTO `priorities` (`text`, `power`, `color`) VALUES
('highest', 1, 'red'),
('high', 2, 'pink'),
('medium', 3, 'orange'),
('low', 4, 'green'),
('lowest', 5, 'success');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `status`
--

CREATE TABLE `status` (
`status_id` int(10) unsigned NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Zrzut danych tabeli `status`
--

INSERT INTO `status` (`status_id`, `text`) VALUES
(0, 'todo'),
(1, 'pending'),
(2, 'done');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tasks`
--

CREATE TABLE `tasks` (
`task_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `priority_power` int(10) unsigned NOT NULL,
  `status_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=31 ;

--
-- Zrzut danych tabeli `tasks`
--

INSERT INTO `tasks` (`task_id`, `title`, `priority_power`, `status_id`) VALUES
(11, 'Task z postmana2', 5, 0),
(15, 'dsaas', 4, 2),
(21, 'mmmmm', 3, 1),
(28, 'sssss', 5, 2),
(29, 'WWW...', 3, 2);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `descriptions`
--
ALTER TABLE `descriptions`
 ADD PRIMARY KEY (`desc_id`), ADD KEY `descriptions_task_id_foreign` (`task_id`);

--
-- Indexes for table `priorities`
--
ALTER TABLE `priorities`
 ADD PRIMARY KEY (`power`), ADD KEY `power` (`power`), ADD KEY `power_2` (`power`), ADD KEY `power_3` (`power`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
 ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
 ADD PRIMARY KEY (`task_id`), ADD KEY `status_id` (`status_id`), ADD KEY `priority_power` (`priority_power`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `descriptions`
--
ALTER TABLE `descriptions`
MODIFY `desc_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT dla tabeli `priorities`
--
ALTER TABLE `priorities`
MODIFY `power` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT dla tabeli `status`
--
ALTER TABLE `status`
MODIFY `status_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `tasks`
--
ALTER TABLE `tasks`
MODIFY `task_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `tasks`
--
ALTER TABLE `tasks`
ADD CONSTRAINT `tasks_ibfk_2` FOREIGN KEY (`status_id`) REFERENCES `status` (`status_id`) ON DELETE CASCADE ON UPDATE CASCADE;
