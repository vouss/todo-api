-- phpMyAdmin SQL Dump
-- version 4.2.5
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Czas generowania: 23 Maj 2015, 12:56
-- Wersja serwera: 5.5.38
-- Wersja PHP: 5.5.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Baza danych: `todo`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `descriptions`
--

CREATE TABLE `descriptions` (
`desc_id` int(10) unsigned NOT NULL,
  `task_id` int(10) unsigned NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Zrzut danych tabeli `descriptions`
--

INSERT INTO `descriptions` (`desc_id`, `task_id`, `text`) VALUES
(0, 0, 'Przykładowy opis zadania1'),
(1, 1, 'Przykładowy opis zadania2'),
(2, 2, 'Przykładowy opis zadania3'),
(3, 3, 'Przykładowy opis zadania4'),
(4, 4, 'Przykładowy opis zadania5');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_05_23_103539_status_table', 1),
('2015_05_23_103633_priorities_table', 1),
('2015_05_23_103706_task_table', 1),
('2015_05_23_103731_descriptions_table', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `priorities`
--

CREATE TABLE `priorities` (
`priority_id` int(10) unsigned NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `power` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Zrzut danych tabeli `priorities`
--

INSERT INTO `priorities` (`priority_id`, `text`, `power`) VALUES
(0, 'highest', 1),
(1, 'high', 2),
(2, 'medium', 3),
(3, 'low', 4),
(4, 'lowest', 5);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `status`
--

CREATE TABLE `status` (
`status_id` int(10) unsigned NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Zrzut danych tabeli `status`
--

INSERT INTO `status` (`status_id`, `text`) VALUES
(0, 'todo'),
(1, 'pending'),
(2, 'done');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tasks`
--

CREATE TABLE `tasks` (
`task_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `priority_id` int(10) unsigned NOT NULL,
  `status_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Zrzut danych tabeli `tasks`
--

INSERT INTO `tasks` (`task_id`, `title`, `priority_id`, `status_id`) VALUES
(0, 'Przykładowe zadanie 1', 0, 0),
(1, 'Przykładowe zadanie 2', 0, 0),
(2, 'Przykładowe zadanie 3', 0, 0),
(3, 'Przykładowe zadanie 4', 0, 0);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `descriptions`
--
ALTER TABLE `descriptions`
 ADD PRIMARY KEY (`desc_id`), ADD KEY `descriptions_task_id_foreign` (`task_id`);

--
-- Indexes for table `priorities`
--
ALTER TABLE `priorities`
 ADD PRIMARY KEY (`priority_id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
 ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
 ADD PRIMARY KEY (`task_id`), ADD KEY `priority_id` (`priority_id`), ADD KEY `status_id` (`status_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `descriptions`
--
ALTER TABLE `descriptions`
MODIFY `desc_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT dla tabeli `priorities`
--
ALTER TABLE `priorities`
MODIFY `priority_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT dla tabeli `status`
--
ALTER TABLE `status`
MODIFY `status_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT dla tabeli `tasks`
--
ALTER TABLE `tasks`
MODIFY `task_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `tasks`
--
ALTER TABLE `tasks`
ADD CONSTRAINT `tasks_ibfk_3` FOREIGN KEY (`task_id`) REFERENCES `descriptions` (`desc_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tasks_ibfk_1` FOREIGN KEY (`priority_id`) REFERENCES `priorities` (`priority_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tasks_ibfk_2` FOREIGN KEY (`status_id`) REFERENCES `status` (`status_id`) ON DELETE CASCADE ON UPDATE CASCADE;
