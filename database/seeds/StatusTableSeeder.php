<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Status as Status;

class StatusTableSeeder extends Seeder {

    public function run()
    {
        DB::table('status')->delete();

        Status::create(['text' => 'todo']);
        Status::create(['text' => 'pending']);
        Status::create(['text' => 'done']);

        $this->command->info('Status Table seeded! :)');
    }

}