<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Task as Task;

class TaskTableSeeder extends Seeder {

    public function run()
    {
        DB::table('tasks')->delete();

        Task::create([ "title" => "Przykładowe zadanie 1", "priority_id" => 1, "status_id" => "8"]);
        Task::create([ "title" => "Przykładowe zadanie 2", "priority_id" => 2, "status_id" => "8"]);
        Task::create([ "title" => "Przykładowe zadanie 3", "priority_id" => 2, "status_id" => "8"]);
        Task::create([ "title" => "Przykładowe zadanie 4", "priority_id" => 3, "status_id" => "7"]);
        Task::create([ "title" => "Przykładowe zadanie 5", "priority_id" => 3, "status_id" => "7"]);
        Task::create([ "title" => "Przykładowe zadanie 6", "priority_id" => 4, "status_id" => "8"]);

        $this->command->info('Tasks Table seeded! :)');
    }

}