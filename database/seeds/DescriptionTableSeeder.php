<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Description as Description;

class DescriptionTableSeeder extends Seeder {

    public function run()
    {
        DB::table('descriptions')->delete();

        Description::create([ "task_id" => "0", "text" => "Przykładowy opis zadania1"]);
        Description::create([ "task_id" => "1", "text" => "Przykładowy opis zadania2"]);
        Description::create([ "task_id" => "2", "text" => "Przykładowy opis zadania3"]);
        Description::create([ "task_id" => "3", "text" => "Przykładowy opis zadania4"]);
        Description::create([ "task_id" => "4", "text" => "Przykładowy opis zadania5"]);

        $this->command->info('Descriptions Table seeded! :)');
    }

}