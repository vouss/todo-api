<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Priority as Priority;

class PriorityTableSeeder extends Seeder {

    public function run()
    {
        DB::table('priorities')->delete();

        Priority::create([ "text" => "highest", "power" => 1]);
        Priority::create([ "text" => "high", "power" => 2]);
        Priority::create([ "text" => "medium", "power" => 3]);
        Priority::create([ "text" => "low", "power" => 4]);
        Priority::create([ "text" => "lowest", "power" => 5]);

        $this->command->info('Priorities Table seeded! :)');
    }

}