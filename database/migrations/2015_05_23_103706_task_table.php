<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TaskTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('tasks', function(Blueprint $table)
        {
            $table->increments('task_id');
            $table->string('title');
            $table->integer('priority_power')->unsigned();
            $table->foreign('priority_power')->references('power')->on('priorities');
            $table->integer('status_id')->unsigned();
            $table->foreign('status_id')->references('status_id')->on('status');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tasks');
	}

}
