<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model {

    protected $fillable = ['title'];
    protected $primaryKey = 'task_id';
    public $timestamps = false;

    public function description() {
        return $this->hasOne('App\Description', 'task_id', 'task_id');
    }

    public function priority() {
        return $this->hasOne('App\Priority', 'power', 'priority_power');
    }

    public function status() {
        return $this->hasOne('App\Status', 'status_id', 'status_id');
    }
}
