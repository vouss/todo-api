<?php namespace App\Http\Controllers;

use App\Description;
use App\Priority;
use App\Status;
use App\Task;
use Illuminate\Http\Request;

class TaskController extends Controller {

    public function indexTask() {


        if($tasks = Task::with('description','priority', 'status')->get()) {
            return response()->json($tasks,200);
        };
            return response()->json(null, 500);
    }

    public function saveTask(Request $request) {

        if ($request->isJson())  {
            $task = new Task();
            $task->title = $request["title"];
            $task->priority_power = $request["priority_power"];
            $task->status_id = $request["status_id"];

            if(!$task->save()) {
                return response()->json(null,500);
            };
            $description = new Description();
            $description->task_id = $task->task_id;
            $description->text = $request["description"];

            if($description->save()) {
                return response()->json(null,200);
            }
            return response()->json(null,500);
        }

    }

    public function getTask($id) {
        $task = Task::with('description','priority', 'status')->find($id);

        return response()->json($task);
    }

    public function deleteTask($id) {
        $task = Task::find($id);

        $task->delete();

        return response()->json(null,200);
    }

    public function updateTask(Request $request, $id) {

        $task = Task::find($id);
        if($request["status_change"]!=9) {
            $task->status_id = $request["status_change"];
            return ($task->save())? response()->json(null,200) : response()->json(null,500);
        }
        $task->title = $request["title"];
        $task->priority_power = $request["priority_power"];
        return ($task->save())? response()->json(null,200) : response()->json(null,500);
    }
}
