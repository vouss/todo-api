<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$app->get('/', function() use ($app) {
    return $app->welcome();
});

$app->get('/api/task', 'App\Http\Controllers\TaskController@indexTask');
$app->get('/api/task/{id}', 'App\Http\Controllers\TaskController@getTask');
$app->post('/api/task', 'App\Http\Controllers\TaskController@saveTask');
$app->delete('/api/task/{id}', 'App\Http\Controllers\TaskController@deleteTask');
$app->put('/api/task/{id}', 'App\Http\Controllers\TaskController@updateTask');