<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Description extends Model {

    protected $fillable = ['text'];
    protected $primaryKey = 'desc_id';
    public $timestamps = false;
}
