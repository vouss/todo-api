<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model {

    protected $fillable = ['name'];
    protected $primaryKey = 'status_id';
    protected $table = 'status';
    public $timestamps = false;

}
