<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Priority extends Model {

    protected $fillable = ['text'];
    protected $primaryKey = 'power';
    public $timestamps = false;

}
